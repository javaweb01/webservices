/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.employee;
import com.opensymphony.xwork2.ActionSupport;
import entity.Employee;
import java.util.List;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Admin
 */
public class edit extends ActionSupport {
    
    public edit() {
    }
    String id;
    String name;
    String salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
   
    
    public String execute() throws Exception {
       employee em=new employee();
      Employee employee=new Employee();
      employee.setId(Integer.parseInt(id));
      employee.setName(name);
      employee.setSalary(Integer.parseInt(salary));
      em.edit_XML(employee, id);
      return SUCCESS;
    }
    
}
