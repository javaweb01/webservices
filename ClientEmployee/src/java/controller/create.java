/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.employee;
import com.opensymphony.xwork2.ActionSupport;
import entity.Employee;

/**
 *
 * @author Admin
 */
public class create extends ActionSupport {
    
    public create() {
    }
    String name;
    String salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
    public String execute() throws Exception {
          Employee e=new Employee();
        e.setId(1);
        e.setName(name);
        e.setSalary(Integer.parseInt(salary));
        employee a=new employee();
        a.create_XML(e);
        return SUCCESS;
    }
    
}
