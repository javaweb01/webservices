/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import client.employee;
import com.opensymphony.xwork2.ActionSupport;
import entity.Employee;
import java.util.List;
import javax.ws.rs.core.GenericType;

/**
 *
 * @author Admin
 */
public class ViewAll extends ActionSupport {
       private List<Employee> employee;
    public ViewAll() {
    }

    public List<Employee> getEmployee() {
        return employee;
    }

    public void setEmployee(List<Employee> employee) {
        this.employee = employee;
    }
    
    public String execute() throws Exception {
         GenericType<List<Employee>> list=new GenericType<List<Employee>>(){};
         employee em=new employee();
    employee=em.findAll_XML(list);
         
       return SUCCESS;
    }
    
}
