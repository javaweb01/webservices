<%-- 
    Document   : history
    Created on : Jul 17, 2018, 10:33:28 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <s:form id="studentForm" class="studentForm" theme="simple">
    <div class="error-msg">
      <s:actionerror />
    </div>
    <table class="list">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Salary</th>
         
          
          
          
        </tr>
      </thead>
      <!-- show list student -->
      <tbody>
        <s:iterator value="employee" id="em" status="st">
          <tr>
            <td><s:property value="#em.id" /></td>
            <td><s:property value="#em.name" /></td>
            <td><s:property value="#em.salary"/></td>
            <td> <div class="control-btn">
                    <td><a href="id=<s:property value="#em.id?"/>?name=<s:property value="#em.name?"/>?salary=<s:property value="#em.salary"/>">edit</a></td>

          </tr>
        </s:iterator>
      </tbody>
    </table>
   
</body>
</html>
